from django.conf import settings
from django.db import models


class Website(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_update=models.CASCADE,
                              on_delete=models.CASCADE);
    name = models.CharField(max_length=150)
    origin = models.CharField(max_length=250)
    slug = models.SlugField(max_length=150)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Page(models.Model):
    website = models.ForeignKey(Website,
                                on_update=models.CASCADE,
                                on_delete=models.CASCADE)
    identifier = models.CharField(max_length=150)
    url = models.CharField(max_length=2000)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Comment(models.Model):
    page = models.ForeignKey(Page,
                             on_update=models.CASCADE,
                             on_delete=models.CASCADE)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_update=models.CASCADE,
                                on_delete=models.CASCADE)
    content = models.CharField(max_length=4000)
    depth = models.IntegerField()
    parent = models.ForeignKey(Comment,
                               on_update=models.CASCADE,
                               on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
